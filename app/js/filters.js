$(document).ready(function() {
    var filter = document.querySelectorAll('.amount-aircraft>a');
    for (var i of filter) {
        i.addEventListener('click', tabsAircraft);

    }
    //filter[0].click();
});
function tabsAircraft(e) {
    e.preventDefault();
    var filter = document.querySelectorAll('.amount-aircraft>a');
    for (var i of filter) {
        i.classList.remove('active');
    }
    this.classList.toggle('active');
    var forTabs = document.querySelectorAll(".in-air-wrap .item");
    for (var i=0, item=forTabs.length; i<item; i++) {
        if ($(this).attr('id') == 'total') {
            $(forTabs[i]).show();
        } else {
            $(forTabs[i]).hide();
        }
        
        if ( $(forTabs[i]).attr('data-statuses') == $(this).attr('data-tab') ) {
            $(forTabs[i]).show();
        }
    }
}



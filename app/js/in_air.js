$(document).ready(function() {
    
});
// Fly ajax
function getFly() {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: "get",
            url: "https://flytimeserver.herokuapp.com/fly/",
            success: function(msg) {
                resolve(msg);
            },
            error: function() {
                reject('error get');
            }
        });
    });
}

getFly().then(function(data) {
    FlyRender(data)
});

function FlyRender(msg) {
    var $str = msg;
    var airTrain = '';

    for (var i=0, item=$str.length; i<item; i++) {
        // date
        var time = $str[i].start_datetime;
        var dateH = new Date(time).getHours();
        var dateM = new Date(time).getMinutes();
        // end date
        var btnLanding = '';
        var btnSeparate = '';
        var btnName = '';
        var btnAirTrain = '';
        var btnLandingAdd = '';
        if ($str[i].statuses == '1') {
            btnAirTrain += 'del-landing-btn';
            btnSeparate += 'btn-separate';
            btnName += 'Отцепка';
        } else if ($str[i].statuses == '1,2,5') {
            btnLandingAdd += 'add-landing-btn';
            btnLanding += 'btn-landing';
            btnName += 'Посадка';
        } else if ($str[i].statuses == '1,2') {
            btnLandingAdd += 'add-landing-btn';
            btnLanding += 'btn-landing';
            btnName += 'Посадка';
        } else if ($str[i].statuses == '1,2,4') {
            btnLandingAdd += 'add-landing-btn';
            btnLanding += 'btn-landing';
            btnName += 'Посадка';
        }

        airTrain +='<div class="item" data-statuses="'+ $str[i].statuses +'" >' +
            '<div class="airplane-info">' +
            '<div class="info-mark airplane-m">' +
            '<p class="type" data-planer_info-id="'+ $str[i].planer_info.id +'">'+ $str[i].planer_info.name + '</p>' +
            '<p class="model">'+ $str[i].planer_info.description +'</p>' +
            '</div>' +
            '<div class="info-cadet airplane-c">' +
            '<span class="cadet-number">'+ $str[i].planer_pilot_info.pilot_code + '</span>' +
            '<div>' +
            '<p class="cadet-name" data-planer_pilot_info-id="'+ $str[i].planer_pilot_info.id +'">'+ $str[i].planer_pilot_info.name +'</p>' +
            '<p class="cadet-air-time"><span class="time">'+ dateH +' час. '+ dateM + ' мин. ' + '</span>'+ 'в воздухе' +'</p>' +
            '</div>' +
            '</div>' +
            '<div class="info-instructor airplane-instr">' +
            '<span class="instructor-number">'+ $str[i].instructor_info.pilot_code + '</span>' +
            '<div>' +
            '<p class="instructor-name" data-instructor_info-id="'+ $str[i].instructor_info.id +'">'+ $str[i].instructor_info.name +'</p>' +
            '<p class="instructor-exercises" data-exercise_info-id="'+ $str[i].exercise_info.id +'">'+ 'Упражнение №' +
            '<span class="exercises-number">'+ $str[i].exercise_info.name +'</span>' +
            '<span class="exercises-type"></span>' +
            '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="item-btn '+ btnLanding +' '+ btnLandingAdd +'">' +
            '<button class="btn '+ btnSeparate +'">'+ btnName + '</button>' +
            '</div>' +
            '<div class="sailplane-info">' +
            '<div class="info-mark sailplane-m">' +
            '<p class="type" data-plane_info-id="'+ $str[i].plane_info.id +'">'+ $str[i].plane_info.name + '</p>' +
            '<p class="model">'+ $str[i].plane_info.description +'</p>' +
            '</div>' +
            '<div class="info-cadet sailplane-c">' +
            '<span class="cadet-number">'+ $str[i].plane_pilot_info.pilot_code + '</span>' +
            '<div>' +
            '<p class="cadet-name" data-plane_pilot_info-id="'+ $str[i].plane_pilot_info.id +'">'+ $str[i].plane_pilot_info.name +'</p>' +
            '<p class="cadet-air-time"><span class="time">'+ dateH +' час. '+ dateM + ' мин. ' +'</span>'+ 'в воздухе' +'</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="item-btn '+ btnLanding +' '+ btnAirTrain +'">' +
            '<button class="btn '+ btnSeparate +'">'+ btnName + '</button>' +
            '</div>' +
            '<span class="ion-edit"></span>' +
            '</div>';
    }
    $('.in-air-wrap').html(airTrain);

    var el = document.getElementsByClassName('item');
    for (var i=0, elem=el.length; i<elem; i++) {
        if( el[i].getAttribute('data-statuses').indexOf('5') != -1 ) {
            $(el[i]).attr('data-type', 'litak');
            $(el[i]).find('.airplane-info, .add-landing-btn').remove();
        } else if (el[i].getAttribute('data-statuses').indexOf('4') != -1) {
            $(el[i]).attr('data-type', 'planer');
            $(el[i]).find('.sailplane-info').remove();
            $(el[i]).find('.add-landing-btn').remove();
        } else if (el[i].getAttribute('data-statuses').indexOf('2') != -1) {
            $(el[i]).clone().appendTo('.in-air-wrap').find('.airplane-info, .add-landing-btn').remove();
            $(el[i]).find('.sailplane-info').remove();
            $(el[i]).find('.add-landing-btn').remove();
        } else if (el[i].getAttribute('data-statuses').indexOf('1') != -1) {
            $(el[i]).find('.del-landing-btn').remove();
        }
    }
    counter();
}

function counter() {
    $('.baidge').eq(0).find('span').text( '('+$('.in-air-wrap .item[data-statuses]').length+')' );
    $('.baidge').eq(1).find('span').text( '('+$('.in-air-wrap .item[data-statuses="1"]').length+')' );
    $('.baidge').eq(2).find('span').text( '('+$('.in-air-wrap .item[data-statuses="1,2,5"]').length+')' );

    //console.log( $('.in-air-wrap .item[data-statuses="1,2"]').length );
    //console.log( $('.in-air-wrap .item[data-statuses="1,2,4"]').length );
}
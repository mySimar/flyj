$(document).ready(function() {
    $('#planer, #plane, #planer_pilot, #plane_pilot, #instructor, #exercise').on('input', searchItems);

    $('.ion-close-round').click(deleteEl);
});

$(document).on('click', '.autocomplete>p', function () {
    var textEl = $(this).text();
    var el =  $(this).parent().prev();
    el.val(textEl);
    if($(this).attr('data-is_double')=='false') {
        $('#instructor').prop('disabled', true);
    } else {
        $('#instructor').removeProp('disabled');
    }
});

// Planer number ajax
$.ajax({
    type: "get",
    url: "https://flytimeserver.herokuapp.com/planer/",
    success: function(msg){
        var planerNumber = isNotFly(msg);
        var planerNumDiv = document.getElementsByClassName('autocomplete')[0];
        for (var i=0, planerN=planerNumber.length; i<planerN; i++) {
            var p = document.createElement('p');
            planerNumDiv.appendChild(p);
            p.innerHTML=planerNumber[i].name;
            if (planerNumber[i].is_double) {
                p.setAttribute('data-is_double', true);
            } else {
                p.setAttribute('data-is_double', false);
            };
        }
    }
});

// Plane number ajax
$.ajax({
    type: "get",
    url: "https://flytimeserver.herokuapp.com/plane/",
    success: function(msg){
        var planeNumber = isNotFly(msg);
        var planeNumberName = planeNumber.map(function(itemId) {
            return itemId.name;
        });
        var planeNumDiv = document.getElementsByClassName('autocomplete')[4];
        for (var i=0, planeNumN=planeNumberName.length; i<planeNumN; i++) {
            var p = document.createElement('p');
            planeNumDiv.appendChild(p);
            p.innerHTML=planeNumberName[i];
        }
    }
});

// Pilot ajax
$.ajax({
    type: "get",
    url: "https://flytimeserver.herokuapp.com/pilots/",
    success: function(msg){
        // Planer pilot ajax
        var pilotPlaner = msg.filter(function(item) {
            return !item.in_fly;
        });
        var pilPlanerName = pilotPlaner.map(function(itemId) {
                return itemId.pilot_code+' '+itemId.name;
        });
        var planerPilDiv = document.getElementsByClassName('autocomplete')[1];
        for (var i=0, pilPlanerN=pilPlanerName.length; i<pilPlanerN; i++) {
            var p = document.createElement('p');
            planerPilDiv.appendChild(p);
            p.innerHTML=pilPlanerName[i];
        }

        // Plane pilot ajax
        var pilotPlane = msg.filter(function(item) {
            return !item.in_fly && item.is_pilot_of_plane;
        });
        var pilPlaneName = pilotPlane.map(function(itemId) {
            return itemId.pilot_code+' '+itemId.name;
        });
        var planePilDiv = document.getElementsByClassName('autocomplete')[5];
        for (var i=0, pilPlaneN=pilPlaneName.length; i<pilPlaneN; i++) {
            var p = document.createElement('p');
            planePilDiv.appendChild(p);
            p.innerHTML=pilPlaneName[i];
        }

        // Planer instructor ajax
        var instPlaner = msg.filter(function(item) {
            return !item.in_fly && item.is_instructor;
        });
        var instName = instPlaner.map(function(itemId) {
            return itemId.pilot_code+' '+itemId.name;
        });
        var planerInstDiv = document.getElementsByClassName('autocomplete')[2];
        for (var i=0, instN=instName.length; i<instN; i++) {
            var p = document.createElement('p');
            planerInstDiv.appendChild(p);
            p.innerHTML=instName[i];
        }
    }
});

// Exercise planer
$.ajax({
    type: "get",
    url: "https://flytimeserver.herokuapp.com/exercise/",
    success: function(msg){
        var exercise = msg.map(function(item) {
            return item.name+' '+item.description;
        });
        var exeDiv = document.getElementsByClassName('autocomplete')[3];
        for (var i=0, exerciseDiv=exercise.length; i<exerciseDiv; i++) {
            var p = document.createElement('p');
            exeDiv.appendChild(p);
            p.innerHTML=exercise[i];
        }
    }
});

// Event
function searchItems() {
    var items = $(this).next().children(); //autocomplite p
    var findDivErrorP = $(this).parent().find('.error-empty-search').children(); // error p
    console.log(findDivErrorP);
    for(var i=0, item=items.length; i<item; i++) {
        var itemIndex = items[i].innerText.toLowerCase().indexOf(this.value.toLowerCase());
        if(this.value.length>0) {
            if(itemIndex > -1) {
                items[i].style.display="block";
                findDivErrorP.css('display', 'none');

            } else {
                items[i].style.display="none";
                findDivErrorP.css('display', 'block');
            }
        } else {
            items[i].style.display="none";
            findDivErrorP.css('display', 'none');

        }
    }
}

// Event delete value
function deleteEl() {
    var input = $(this).parent().find('input');
    input.val('');
}

function isNotFly(array) {
 return array.filter(function(item) {
    return !item.in_fly;
 });
}
function vehicleName(array) {
    return array.map(function(item) {
        return item.name;
    });
}
